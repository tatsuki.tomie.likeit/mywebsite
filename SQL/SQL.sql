
USE minnnanogakuhu;

CREATE TABLE composer(id int PRIMARY KEY AUTO_INCREMENT,name varchar(200) NOT NULL,junban int NOT NULL);

CREATE TABLE style(id int PRIMARY KEY,name varchar(200)NOT NULL,junban int NOT NULL);

CREATE TABLE genre(id int PRIMARY KEY,name varchar(200)NOT NULL,junban int NOT NULL);

CREATE TABLE situation(id int PRIMARY KEY AUTO_INCREMENT,name varchar(200) UNIQUE NOT NULL,junban int NOT NULL);

CREATE TABLE instrument(id int PRIMARY KEY,name varchar(200) NOT NULL,junban int NOT NULL);

CREATE TABLE detail(id int  PRIMARY KEY AUTO_INCREMENT,score_name varchar(200) NOT NULL,editor varchar(200) NOT NULL,publisher_info varchar(200) NOT NULL,
copyright varchar(30) default 'Public Domain',junban int NOT NULL);

CREATE TABLE score(
id int PRIMARY KEY AUTO_INCREMENT
,name varchar(200) 
,detail_id int
,movie_file varchar(200) 
,pdf_file varchar(200)
,composer_id int
,style_id int 
,genre_id int 
,situation_id int 
,instrument_id int 
,FOREIGN KEY(detail_id) REFERENCES detail(id)
,FOREIGN KEY(composer_id)REFERENCES composer(id)
,FOREIGN KEY(style_id) REFERENCES style(id)
,FOREIGN KEY(genre_id) REFERENCES genre(id)
,FOREIGN KEY(situation_id) REFERENCES situation(id)
,FOREIGN KEY(instrument_id) REFERENCES instrument(id) );
