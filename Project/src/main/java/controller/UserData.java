package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BrowseDAO;
import dao.UserDao;
import model.Browse;
import model.User;
import util.PasswordEncorder;

/**
 * ユーザー情報画面
 *
 * @author d-yamaguchi
 *
 */
@WebServlet("/UserData")
public class UserData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// セッション開始
		HttpSession session = request.getSession();
		System.out.println("try前");
		try {

			// 入力された内容に誤りがあったとき等に表示するエラーメッセージを格納する

			System.out.println("after");
			int userId = (int) session.getAttribute("userId");
			UserDao userDao = new UserDao();
			User userUpdate = userDao.findById(userId);

			BrowseDAO browsedao = new BrowseDAO();
			List<Browse> bdbList = browsedao.getBrowseDataByUserId(userId);
			request.setAttribute("userUpdate", userUpdate);
			request.setAttribute("bdbList", bdbList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdata.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String id = request.getParameter("user-id");
		String loginId = request.getParameter("user-loginid");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("password-confirm");
		String name = request.getParameter("user-name");

		if (!(password.equals(passwordConfirm)) || name.equals("") || password.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("name", name);
			int userId = (int) session.getAttribute("userId");
			UserDao userDao = new UserDao();
			User userUpdate = userDao.findById(userId);
			request.setAttribute("userUpdate", userUpdate);

			BrowseDAO browsedao = new BrowseDAO();
			List<Browse> bdbList;
			try {
				bdbList = browsedao.getBrowseDataByUserId(userId);
				request.setAttribute("bdbList", bdbList);
			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdata.jsp");
			dispatcher.forward(request, response);
			return;

		}

		UserDao userDao = new UserDao();
		String encodeStr = PasswordEncorder.encordPassword(password);
		userDao.userUpdate(id, loginId, encodeStr, name);

		response.sendRedirect("UserData");
	}

}

