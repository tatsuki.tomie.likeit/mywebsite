package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ScoreDao;
import model.Score;
import model.orderIntoName;

/**
 * Servlet implementation class SearchResult
 */
@WebServlet("/SearchResult")
public class SearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static int PAGE_MAX_ITEM_COUNT = 20;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String junban = request.getParameter("junban");
		int order = Integer.parseInt(junban);

		ScoreDao scoredao = new ScoreDao();
		List<Score> scoreList = scoredao.findByOrder(order);
		String name = orderIntoName.orderIntoName(order);
		
		if (scoreList.size() == 0) {
			request.setAttribute("name", name);
			request.setAttribute("errMsg", "comming soon...");
			System.out.println("size 0");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/search_results.jsp");
			dispatcher.forward(request, response);
		} else {
			request.setAttribute("name", name);
			request.setAttribute("scoreList", scoreList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/search_results.jsp");
			dispatcher.forward(request, response);
		}
	
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String searchWord = request.getParameter("search_word");
		request.setAttribute("searchWord", searchWord);

		List<Score> scoreList = null;
		try {
			scoreList = ScoreDao.getScoresByScoreName(searchWord);
			request.setAttribute("scoreList", scoreList);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/search_results.jsp");
		dispatcher.forward(request, response);

	}
}
