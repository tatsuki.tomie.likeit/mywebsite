package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;
import util.PasswordEncorder;

@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserAddServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");

		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("user-loginid");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("password-confirm");
		String name = request.getParameter("user-name");

		UserDao userDao = new UserDao();

		User user = userDao.findByLoginInfo(loginId, password);

		if (!(user == null) || loginId.equals("") || password.equals("") || name.equals("")
				|| (!(password.equals(passwordConfirm)))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);

			return;

		}

		String encodeStr = PasswordEncorder.encordPassword(password);

		userDao.userAdd(loginId, name, encodeStr);

		response.sendRedirect("Mainpage");

		return;

	}
}
