package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BrowseDAO;
import dao.ScoreDao;
import model.Browse;
import model.Detail;
import model.User;

/**
 * Servlet implementation class ResultDetail
 */
@WebServlet("/ResultDetail")
public class ResultDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResultDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String a = request.getParameter("id");
		int id = Integer.parseInt(a);
		ScoreDao scoredao = new ScoreDao();
		Detail detail = scoredao.findById(id);
		System.out.println(detail);
		request.setAttribute("detail", detail);
		User user = (User) session.getAttribute("userInfo");

		if (user != null) {
			int userId = (int) session.getAttribute("userId");
			request.setAttribute("userId", userId);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/results_detail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int detailId = Integer.parseInt(request.getParameter("detail-id"));
		int userId = Integer.parseInt(request.getParameter("userId"));

		System.out.println("test1");
		BrowseDAO browseDAO = new BrowseDAO();
		Browse bdb = new Browse();
		bdb.setDetailId(detailId);
		bdb.setUserId(userId);
		try {
			browseDAO.insertData(bdb);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
			System.out.println("test2");

			String url1 = "http://localhost:8080/MyWebSite/ResultDetail?id=";
			Integer i = Integer.valueOf(detailId);
			String url2 = i.toString();
			String url3 = url1 + url2;
			System.out.println(url3);
			response.sendRedirect(url3);


	}

}
