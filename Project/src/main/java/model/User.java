package model;

import java.io.Serializable;

public class User implements Serializable {
	private String loginId;
	private String password;
	private String name;
	private int id;

	public User(String loginIdData, String nameData) {
		this.loginId = loginIdData;
		this.name = nameData;
	}

	public User(int id, String loginId, String name, String password) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}