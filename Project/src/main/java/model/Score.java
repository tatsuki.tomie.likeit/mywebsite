package model;
import java.io.Serializable;

public class Score implements Serializable {
	private int id;
	private String name;
	private String movieFile;
	private String pdfFile;
	private String composerName;
	private String situationName;
	
	public Score() {
	};

	public Score(int id, String name, String movieFile, String pdfFile, String composerName, String situationName) {
		this.id = id;
		this.name = name;
		this.movieFile = movieFile;
		this.pdfFile = pdfFile;
		this.composerName = composerName;
		this.situationName = situationName;

	}

	public Score(int id2, String name2, String movieFile2, String pdfFile2) {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMovieFile() {
		return movieFile;
	}

	public void setMovieFile(String movieFile) {
		this.movieFile = movieFile;
	}

	public String getPdfFile() {
		return pdfFile;
	}

	public void setPdfFile(String pdfFile) {
		this.pdfFile = pdfFile;
	}

	public String getComposerName() {
		return composerName;
	}

	public void setComposerName(String composerName) {
		this.composerName = composerName;
	}

	public String getSituationName() {
		return situationName;
	}

	public void setSituationName(String situationName) {
		this.situationName = situationName;
	}



}
