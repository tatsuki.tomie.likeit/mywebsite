
package model;
import java.io.Serializable;

public class Browse implements Serializable {
	private int id;
	private int userId;
	private int detailId;
	private String scoreName;
	private String pdfFile;

	public int getDetailId() {
		return detailId;
	}

	public void setDetailId(int scoreId) {
		this.detailId = scoreId;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getScoreName() {
		return scoreName;
	}

	public void setScoreName(String scoreName) {
		this.scoreName = scoreName;
	}

	public String getPdfFile() {
		return pdfFile;
	}

	public void setPdfFile(String pdfFile) {
		this.pdfFile = pdfFile;
	}


}