package model;

import java.io.Serializable;

public class Detail implements Serializable {
	private int id;
	private String scoreName;
	private String composerName;
	private String editor;
	private String publisherInfo;
	private String copyright;
	private String movieFile;
	private String pdfFile;

	public Detail(int id, String scoreName, String composerName, String editor, String publisherInfo, String copyright,
			String movieFile, String pdfFile) {
		this.id = id;
		this.scoreName = scoreName;
		this.composerName = composerName;
		this.editor = editor;
		this.publisherInfo = publisherInfo;
		this.copyright = copyright;
		this.setMovieFile(movieFile);
		this.pdfFile = pdfFile;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getScoreName() {
		return scoreName;
	}

	public void setScoreName(String scoreName) {
		this.scoreName = scoreName;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getPublisherInfo() {
		return publisherInfo;
	}

	public void setPublisherInfo(String publisherInfo) {
		this.publisherInfo = publisherInfo;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public String getPdfFile() {
		return pdfFile;
	}

	public void setPdfFile(String pdfFile) {
		this.pdfFile = pdfFile;
	}

	public String getComposerName() {
		return composerName;
	}

	public void setComposerName(String composerName) {
		this.composerName = composerName;
	}

	public String getMovieFile() {
		return movieFile;
	}

	public void setMovieFile(String movieFile) {
		this.movieFile = movieFile;
	}

}
