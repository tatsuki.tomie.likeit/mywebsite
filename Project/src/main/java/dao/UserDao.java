package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;

public class UserDao {

	public User findByLoginInfo(String loginId, String password) {
	Connection conn =null;
	 try {
	conn = DBManager.getConnection();
	String sql =  "SELECT * FROM user WHERE login_id = ? and password = ?";
	 PreparedStatement pStmt = conn.prepareStatement(sql);
     pStmt.setString(1, loginId);
     pStmt.setString(2, password);
     ResultSet rs = pStmt.executeQuery();

   if(!rs.next()) {
	   return null;
   }
	
	String loginIdData = rs.getString("login_id");
	String nameData = rs.getString("name");
	return new User(loginIdData, nameData);
	

	 }catch (SQLException e) {
       e.printStackTrace();
       return null;
   } finally {

       if (conn != null) {
           try {
               conn.close();
           } catch (SQLException e) {
               e.printStackTrace();
               return null;
           }
       }
   }

}

public int getUserId(String loginId, String encodeStr) throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	try {
		con = DBManager.getConnection();

		st = con.prepareStatement("SELECT * FROM user WHERE login_id = ?");
		st.setString(1, loginId);

		ResultSet rs = st.executeQuery();

		int userId = 0;
		while (rs.next()) {
			userId = rs.getInt("id");
			System.out.println("login succeeded");
		}

		System.out.println("searching userId by loginId has been completed");
		return userId;
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();
		}
	}
}

public User findById(int userId) {
	Connection conn = null;
	try {
		conn = DBManager.getConnection();
		String sql = "SELECT * FROM user WHERE id= ?";
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setInt(1, userId);

		ResultSet rs = pStmt.executeQuery();

		if (!rs.next()) {
			return null;
		}
		int id = rs.getInt("id");
		String loginId = rs.getString("login_id");
		String name = rs.getString("name");
		String password = rs.getString("password");
		User user = new User(id, loginId, name, password);

		return user;

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

}

public void userUpdate(String id, String loginId, String encodeStr, String name) {

	Connection conn = null;
	PreparedStatement pStmt = null;

	try {
		conn = DBManager.getConnection();

		String sql = "UPDATE user SET name = ?,password = ?,login_id = ? WHERE id = ?";

		pStmt = conn.prepareStatement(sql);

		pStmt.setString(1, name);
		pStmt.setString(2, encodeStr);
		pStmt.setString(3, loginId);
		pStmt.setString(4, id);

		pStmt.executeUpdate();

		pStmt.close();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

	}

}

public void userAdd(String loginId, String name, String password) {

	Connection conn = null;
	PreparedStatement pStmt = null;

	try {
		conn = DBManager.getConnection();

		String sql = "INSERT INTO user (login_id,name,password)VALUES(?,?,?)";

		pStmt = conn.prepareStatement(sql);

		pStmt.setString(1, loginId);
		pStmt.setString(2, name);
		pStmt.setString(3, password);

		pStmt.executeUpdate();

		pStmt.close();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

	}
}
}
