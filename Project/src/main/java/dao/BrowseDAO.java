package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Browse;

public class BrowseDAO {


	public void insertData(Browse bdb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO browse(user_id,detail_id) VALUES(?,?)");
			st.setInt(1, bdb.getUserId());
			st.setInt(2, bdb.getDetailId());
			st.executeUpdate();


			System.out.println("inserting buy-datas has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	
	public List<Browse> getBrowseDataByUserId(int userId) throws SQLException {
			Connection con = null;
			List<Browse> bdList = new ArrayList<Browse>();
			try {
				con = DBManager.getConnection();

				String sql = "SELECT * FROM browse JOIN user ON browse.user_id = user.id JOIN detail ON browse.detail_id = detail.id WHERE browse.user_id = ?";


				PreparedStatement pStmt = con.prepareStatement(sql);
				pStmt.setInt(1, userId);


				ResultSet rs = pStmt.executeQuery();

				while (rs.next()) {
					Browse bdb = new Browse();
					bdb.setId(rs.getInt("id"));
					bdb.setDetailId(rs.getInt("detail_id"));
					bdb.setScoreName(rs.getString("score_name"));


					bdList.add(bdb);

				}

			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}

			return bdList;
						
		
		
	}

	public void deleteById(String id) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM browse WHERE id= ?");
			st.setString(1, id);
			st.executeUpdate();

			System.out.println("deleting buy-datas has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}


