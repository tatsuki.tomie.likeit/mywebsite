package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Detail;
import model.Score;

public class ScoreDao {
	public List<Score> findByOrder(int order) {
		Connection conn = null;
		List<Score> scoreList = new ArrayList<Score>();
		String sql = null;
		PreparedStatement pStmt = null;

		try {

			conn = DBManager.getConnection();
			if (order <= 17) {
				sql = "SELECT* FROM score INNER JOIN instrument ON score.instrument_id = instrument.id JOIN composer ON score.composer_id = composer.id JOIN situation ON score.situation_id = situation.id WHERE instrument.junban = ?";
				pStmt = conn.prepareStatement(sql);

				pStmt.setInt(1, order);

			} else if (order > 17 && order <= 31) {
				sql = "SELECT* FROM score INNER JOIN style ON score.style_id = style.id JOIN composer ON score.composer_id = composer.id JOIN situation ON score.situation_id = situation.id WHERE style.junban = ?";

				pStmt = conn.prepareStatement(sql);

				pStmt.setInt(1, order);

			} else if (order > 31 && order < 39) {
				sql = "SELECT* FROM score INNER JOIN genre ON score.genre_id = genre.id JOIN composer ON score.composer_id = composer.id JOIN situation ON score.situation_id = situation.id WHERE genre.junban = ?";
				pStmt = conn.prepareStatement(sql);

				pStmt.setInt(1, order);

			} else{
				sql = "SELECT* FROM score INNER JOIN situation ON score.situation_id = situation.id JOIN composer ON score.composer_id = composer.id   WHERE situation.junban = ?";
				pStmt = conn.prepareStatement(sql);

				pStmt.setInt(1, order);

			}

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("score_id");
				String name = rs.getString("name");
				String movieFile = rs.getString("movie_file");
				String pdfFile = rs.getString("pdf_file");
				String composerName = rs.getString("composer_name");
				String situationName = rs.getString("situation_name");
				Score score = new Score(id, name, movieFile, pdfFile, composerName, situationName);

				scoreList.add(score);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return scoreList;
	}

	public static List<Score> getScoresByScoreName(String searchWord) throws SQLException {
		Connection conn = null;
		List<Score> scoreList = new ArrayList<Score>();
		String sql = null;
		PreparedStatement pStmt = null;
	
		try {
			conn = DBManager.getConnection();

			if (searchWord.length() == 0) {
				// 全検索
				sql = "SELECT * FROM score " + "JOIN composer ON score.composer_id = composer.id "
						+ "JOIN situation ON score.situation_id = situation.id";
				pStmt = conn.prepareStatement(sql);
			
			} else {
				// 商品名検索
				sql = "SELECT * FROM score " + "JOIN composer ON score.composer_id = composer.id "
						+ "JOIN situation ON score.situation_id = situation.id "
						+ "JOIN instrument ON score.instrument_id = instrument.id JOIN style ON score.style_id = style.id JOIN genre ON score.genre_id = genre.id WHERE name like ? OR composer_name like ? OR situation_name like ? OR instrument_name like ? OR style_name like ? OR genre_name like ?";

				pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, "%" + searchWord + "%");
				pStmt.setString(2, "%" + searchWord + "%");
				pStmt.setString(3, "%" + searchWord + "%");
				pStmt.setString(4, "%" + searchWord + "%");
				pStmt.setString(5, "%" + searchWord + "%");
				pStmt.setString(6, "%" + searchWord + "%");
			}
				


			ResultSet rs = pStmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("score_id");
				String name = rs.getString("name");
				String movieFile = rs.getString("movie_file");
				String pdfFile = rs.getString("pdf_file");
				String composerName = rs.getString("composer_name");
				String situationName = rs.getString("situation_name");

				Score score = new Score(id, name, movieFile, pdfFile, composerName, situationName);

				scoreList.add(score);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return scoreList;
	}

	public Detail findById(int id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;
		String sql = null;
		PreparedStatement pStmt = null;
		try {

			conn = DBManager.getConnection();

			sql = "SELECT * FROM score JOIN detail ON score.detail_id = detail.id  JOIN composer ON score.composer_id = composer.id WHERE score_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
		

	   ResultSet rs = pStmt.executeQuery();
		if(!rs.next()){
			return null;
		}
		int id1 = rs.getInt("id");
		 String scoreName = rs.getString("score_name");
			String composerName = rs.getString("composer_name");
		 String editor = rs.getString("editor");
		 String publisherInfo =rs.getString("publisher_info");
		String copyright =rs.getString("copyright");
		String movieFile = rs.getString("movie_file");
		String pdfFile = rs.getString("pdf_file");
		Detail detail = new Detail(id1, scoreName, composerName, editor, publisherInfo, copyright, movieFile, pdfFile);
		
		return detail;

	}catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	

}
}