<%@ page language="java" contentType="text/html;charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ新規登録画面</title>
<!-- BootstrapのCSS読み込み -->
 <link rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css">
    <link rel ="stylesheet" href="<%=request.getContextPath() %>/css/common.css">

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

</head>

<body>

	<!-- header -->
	<header>
		
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container-fluid">
		<div class="row mb-3">
			<div class="col">
				<h1 class="text-center">ユーザ新規登録</h1>
			</div>
		</div>
        
		<div class="row">
			<div class="col-6 offset-3 mb-5">
		 
	           
			</div>
		</div>
			
		<div class="row">
			<div class="col-6 offset-3">
			
			 <c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>
			
			
			     <form action ="UserAddServlet" method ="post">
	                	<div class="form-group row">
						<label for="user-id" class="control-label col-3">ログインID</label>
						<div class="col-9">
							<input type="text" name="user-loginid" id="user-loginid"
								class="form-control" value="${loginId}" >
						</div>
					</div>
					<div class="form-group row">
						<label for="password" class="control-label col-3">パスワード</label>
						<div class="col-9">
							<input type="password" name="password" id="password"
								class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="password-confirm" class="control-label col-3">パスワード(確認)</label>
						<div class="col-9">
							<input type="password" name="password-confirm"
								id="password-confirm" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="user-name" class="control-label col-3">ユーザ名</label>
						<div class="col-9">
							<input type="text" name="user-name" id="user-name"
								class="form-control" value="${name}">
						</div>
					</div>

					<div>
					  
					  
						<button type="submit" value="登録"
							class="btn btn-primary btn-block form-submit">登録</button>
					</div>		
					
					<div class="row mt-3">
						<div class="col">
							<a href="Mainpage">メインページに戻る</a>
						</div>
					</div>
				</form>
			</div>
		</div>
		</div>
	




</body>

</html>