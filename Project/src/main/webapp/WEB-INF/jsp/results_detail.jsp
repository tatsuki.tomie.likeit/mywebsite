<%@ page language="java" contentType="text/html;charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>result_detail</title>
    
    
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css">
    <link rel ="stylesheet" href="<%=request.getContextPath() %>/css/common.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
</head>

<body>
    <!-- ヘッダー -->
    <header>
           <a href= "Mainpage"><h1>みんなの楽譜</h1></a>
           <div class ="container-fluid">
           <div class ="row">
           <div class ="col-5 ml-auto">
            <form method="post" action="SearchResult" class="search_container">
                <input type="text" size="25" placeholder="キーワード検索" name= "search_word">
                <input type="submit" value="&#xf002">
              </form>
          </div>
        
         <div class="col-3">
         <ul class= "menu">
			<c:if test="${userInfo != null}">
			<li class="menu-item"><a href="UserData">ユーザー情報</a></li>
			</c:if>
			<c:choose>
			<c:when test="${userInfo != null}">
			<li>&nbsp;&nbsp;</li>
			<li class="menu-item"><a href="LogoutServlet">ログアウト</a></li>
			</c:when>
			<c:otherwise>
			<li class="menu-item"><a href="LoginServlet">ログイン</a></li>
			</c:otherwise>
			</c:choose>
         </ul>
		</div>
		 </div>
		</div>
    </header>
    <div class="container-fluid" style= "background-color: rgba(229, 230, 230, 0.411) ">
    <div class="container mt-5">
        <div class="">
            <br></br>
            <h5 class="title">${detail.scoreName}</h5>
            <p>${detail.composerName}</p>
        </div>
        <form action ="Pdf" method ="post">
        <input type="hidden" name="detail-id" value="${detail.id}">
        <input type="hidden" name="pdfFile" value="${detail.pdfFile}">
        <button type="submit" 
        class="btn btn-light btn-lg btn-block" role="button">ここをクリックして楽譜を見る（PDF) </button>
        </form>
        <br>詳細
        <table>
            <tbody>
                <tr>
                    <th>編集者</th>
                    <td>${detail.editor}</td>
                </tr>
                 <tr>
                    <th>出版社情報</th>
                    <td>${detail.publisherInfo}</td>
                </tr>
                 <tr>
                    <th>著作権</th>
                    <td>${detail.copyright}</td>
                   
                </tr>
                
            </tbody>
        </table>
         <form action ="ResultDetail" method ="post">
        <c:if test="${userId != null}">
        <br><br>
		 <input type="hidden" name="detail-id" value="${detail.id}">
		 <input type="hidden" name="userId" value="${userId}">
        <button type="submit" class="btn btn-outline-warning btn-sm">お気に入り登録</button>
        </form>
       </c:if>
        <br>
        <br>
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="${detail.movieFile }"></iframe>
          </div>
         </div>
        </div>
        </body>
</html>