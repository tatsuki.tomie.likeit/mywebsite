<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報</title>
 <link rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css">
    <link rel ="stylesheet" href="<%=request.getContextPath() %>/css/common.css">

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
<header>
  <a href= "Mainpage"><h1>みんなの楽譜</h1></a>
</header>
	<br>
	<br>
	<div class="container-fluid">
		<div class="row mb-3">
			<div class="col">
				<h1 class="text-center">ユーザ情報</h1>
			</div>
		</div>
        
		<div class="row">
			<div class="col-6 offset-3 mb-5">
		 
	           
			</div>
		</div>
			
		<div class="row">
			<div class="col-6 offset-3">
			
			 <c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>
					<form action ="UserData" method ="post">
					<input type="hidden" name="user-id" value="${userUpdate.id}">
	                	<div class="form-group row">
						<label for="user-id" class="control-label col-3">ログインID</label>
						<div class="col-9">
							<input type="text" name="user-loginid" id="user-loginid"
								class="form-control" value="${userUpdate.loginId}" >
						</div>
					</div>
					<div class="form-group row">
						<label for="password" class="control-label col-3">パスワード</label>
						<div class="col-9">
							<input type="password" name="password" id="password"
								class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="password-confirm" class="control-label col-3">パスワード(確認)</label>
						<div class="col-9">
							<input type="password" name="password-confirm"
								id="password-confirm" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="user-name" class="control-label col-3">ユーザ名</label>
						<div class="col-9">
							<input type="text" name="user-name" id="user-name"
								class="form-control" value="${userUpdate.name}">
						</div>
					</div>

					<div>
					  
					  
						<button type="submit" value="更新"
							class="btn btn-primary btn-block form-submit">更新</button>
					 </div>
					</form>
			
		
					
					<!--  購入履歴 -->
	<br>
	<br>
<div class="card" style="width: 50rem;">
  <div class="card-header">
    お気に入り
  </div>
  <c:forEach var="Browse" items="${bdbList}">
<ul class="list-group list-group-flush">
    <li class="list-group-item">
    <div style="display:inline-flex">
    <a href= "ResultDetail?id=${Browse.detailId}">${Browse.scoreName}</a>
    &emsp;
    <form action ="BrowseDelete" method ="post">
 	<input type="hidden" name="id" value="${Browse.id}">
        <button type="submit" class="btn btn-link btn-sm">削除</button>
    </form>
    </div>
    </li>
     <c:if test="${Browse.id == null}">
     <ul class="list-group list-group-flush">
      <li class="list-group-item">まだお気に入り登録していません</li>
      </ul>
     </c:if>
 
 </ul>
  </c:forEach>
</div>
</div>
</div>
								
					
	
</body>
</html>