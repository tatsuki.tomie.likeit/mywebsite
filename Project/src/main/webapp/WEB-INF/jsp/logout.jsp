<%@ page language="java" contentType="text/html;charset=UTF-8"
    pageEncoding="UTF-8"%>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<!-- BootstrapのCSS読み込み -->

 <link rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css">
    <link rel ="stylesheet" href="<%=request.getContextPath() %>/css/common.css">

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
<!-- ヘッダー -->
<header>
    <a href= "Mainpage"><h1>みんなの楽譜</h1></a>
        
</header>
	<!-- /header -->

	<!-- body -->
	<br><br><br>
    <div class="row">
        <div class="section"></div>
        <div class="col s8 offset-s4">
            <h4 style=  "text-align: center;">ログアウトしました</h4>
                    
                    <div class="row">
                        <div class="col s12">
                            <p class="center-align">
                                <a href="Mainpage" class="btn btn-outline-primary  col s8 offset-s2">TOPページへ</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>