<%@ page language="java" contentType="text/html;charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>home</title>
    
    
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css">
    <link rel ="stylesheet" href="<%=request.getContextPath() %>/css/common.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
</head>

<body>
    <!-- ヘッダー -->
  <header>
           <a href= "Mainpage"><h1>みんなの楽譜</h1></a>
           <div class ="container-fluid">
           <div class ="row">
           <div class ="col-5 ml-auto">
            <form method="post" action="SearchResult" class="search_container">
                <input type="text" size="25" placeholder="キーワード検索" name= "search_word">
                <input type="submit" value="&#xf002">
              </form>
          </div>
        
         <div class="col-3">
         <ul class= "menu">
			<c:if test="${userInfo != null}">
			<li class="menu-item"><a href="UserData">ユーザー情報</a></li>
			</c:if>
			<c:choose>
			<c:when test="${userInfo != null}">
			<li>&nbsp;&nbsp;</li>
			<li class="menu-item"><a href="LogoutServlet">ログアウト</a></li>
			</c:when>
			<c:otherwise>
			<li class="menu-item"><a href="LoginServlet">ログイン</a></li>
			</c:otherwise>
			</c:choose>
         </ul>
		</div>
		 </div>
		</div>
    </header>
    <div class="container-fluid" style= "background-color: rgba(229, 230, 230, 0.411) ">
    <div class="container mt-5">
        <div class="">
          <br></br>
            <h5 class="title">楽器から選ぶ（ソロ）</h5>
        </div>
        <div class="classified_container">
            <div class="row">
                <div class="col-sm">
                 <ul>
                              <a class = "btn_hover" href= SearchResult?junban=1><li>ピアノ</li></a>
                              <a class = "btn_hover"href= SearchResult?junban=2><li>ギター</li></a>
                                <li>弦楽器</li>
                                 <ul>
                                 <a class = "btn_hover" href= SearchResult?junban=3><li>ヴァイオリン</li></a>
                                  <a class = "btn_hover"href= SearchResult?junban=4><li>ヴィオラ</li></a>
                                  <a class = "btn_hover"href= SearchResult?junban=5><li>チェロ</li></a>
                                  <a class = "btn_hover"href= SearchResult?junban=6><li>コントラバス</li></a>
                                  <a class = "btn_hover"href= SearchResult?junban=7><li>その他</li></a>
                                 </ul>
                            </ul>
                </div>
              
                     <div class="col-sm">
                            <ul>
                                <li>管楽器</li>
                                <ul>
                                  <a class = "btn_hover"href= SearchResult?junban=8><li>フルート</li></a>
                                  <a class = "btn_hover"href= SearchResult?junban=9>  <li>ファゴット</li></a>
                                  <a class = "btn_hover"href= SearchResult?junban=10>  <li>クラリネット</li></a>
                                  <a class = "btn_hover"href= SearchResult?junban=11>    <li>サックス</li></a>
                                    <a class = "btn_hover"href= SearchResult?junban=12>  <li>トランペット</li></a>
                                      <a class = "btn_hover"href= SearchResult?junban=13>  <li>ホルン</li></a>
                                        <a class = "btn_hover"href= SearchResult?junban=14>  <li>トロンボーン</li></a>
                                          <a class = "btn_hover"href= SearchResult?junban=15>  <li>チューバ</li></a>
                                            <a class = "btn_hover"href= SearchResult?junban=16>  <li>ユーフォニアム</li></a>
                                              <a class = "btn_hover"href= SearchResult?junban=17>  <li>その他</li></a>
                                </ul>

                            </ul>
                </div>
             </div>
          </div>
            
                
                           
            <div class="">
                    <br></br>
                    <h5 class="title">演奏方法から選ぶ</h5>
            </div>
                <div class="classified_container">
                    <div class="row">
                        <div class="col-sm">
                            <ul>
                                <li>吹奏楽</li>
                                    <ul>
                                      <a class = "btn_hover" href= SearchResult?junban=18><li>スコア</li></a>
                                      <a class = "btn_hover"href= SearchResult?junban=19> <li>パート譜</li></a>
                                    </ul>
                                <li>アンサンブル</li>
                                    <ul>
                                      <a class = "btn_hover"href= SearchResult?junban=20><li>弦楽アンサンブル（スコア）</li></a>
                                      <a class = "btn_hover"href= SearchResult?junban=21><li>弦楽アンサンブル（パート）</li></a>
                                      <a class = "btn_hover"href= SearchResult?junban=22><li>金管アンサンブル（スコア）</li></a>
                                      <a class = "btn_hover"href= SearchResult?junban=23><li>金管アンサンブル（パート）</li></a>
                                      <a class = "btn_hover"href= SearchResult?junban=24><li>木管アンサンブル（スコア）</li></a>
                                      <a class = "btn_hover"href= SearchResult?junban=25><li>木管アンサンブル（パート）</li></a>
                                      <a class = "btn_hover"href= SearchResult?junban=26><li>その他室内楽</li></a>
                                    </ul>
                            </ul>
                        </div>
                        <div class="col-sm">
                            <ul>
                                <li>オーケストラ
                                    <ul>
                                      <a class = "btn_hover"href= SearchResult?junban=27><li>スコア</li></a>
                                      <a class = "btn_hover"href= SearchResult?junban=28><li>パート譜</li></a>
                                    </ul>
                                </li>
                                <a class = "btn_hover"href= SearchResult?junban=29> <li>声楽</li></a>
                                 <a class = "btn_hover"href= SearchResult?junban=30><li>連弾</li></a>
                                  <a class = "btn_hover"href= SearchResult?junban=31><li>伴奏</li></a>
                            </ul>
                        </div>
                    </div>
                </div>
            <div class="">
                    <br></br>
                    <h5 class="title">ジャンル</h5>
            </div>
             <div class="classified_container">
                <div class="listinline">
                    <ul>
                      <a class = "btn_hover"href= SearchResult?junban=32><div><li>バロック</li></div></a>
                      <a class = "btn_hover"href= SearchResult?junban=33><div><li>古典派</li></div></a>
                      <a class = "btn_hover"href= SearchResult?junban=34><div><li>ロマン派</li></div></a>
                      <a class = "btn_hover"href= SearchResult?junban=35><div><li>近代</li></div></a>
                      <a class = "btn_hover"href= SearchResult?junban=36><div><li>宗教音楽</li></div></a>
                      <a class = "btn_hover"href= SearchResult?junban=37><div><li>ジャズ</li></div></a>
                      <a class = "btn_hover"href= SearchResult?junban=38><div><li>ルネサンス</li></div></a>
                    </ul>
                </div>
             </div>
             <div class="">
                <br></br>
                <h5 class="title">シチュエーション</h5>
                <div class="classified_container">
                    <div class="listinline">
                        <ul>
                          <a class = "btn_hover" href= SearchResult?junban=39><div><li>元気</li></div></a>
                          <a class = "btn_hover" href= SearchResult?junban=40><div><li>悲しい</li></div></a>
                          <a class = "btn_hover" href= SearchResult?junban=41> <div><li>急ぐ</li></div></a>
                          <a class = "btn_hover" href= SearchResult?junban=42><div><li>眠る</li></div></a>
                          <a class = "btn_hover" href= SearchResult?junban=43><div><li>リラックス</li></div></a>
                          <a class = "btn_hover" href= SearchResult?junban=44><div><li>目覚まし</li></div></a>
                          <a class = "btn_hover" href= SearchResult?junban=45><div><li>春</li></div></a>
                          <a class = "btn_hover" href= SearchResult?junban=46><div><li>夏</li></div></a>
                          <a class = "btn_hover" href= SearchResult?junban=47><div><li>秋</li></div></a>
                          <a class = "btn_hover" href= SearchResult?junban=48><div><li>冬</li></div></a>
                        </ul>
                    </div>
                </div>
                        <div class="">
                            <br></br>
                <h5 class="title">おすすめ</h5>
                <div class="card mb-3" style="max-width: 80%; height:170px;">
                    <div class="row no-gutters">
                      <div class="col-md-4">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/UJdlnqDiRnE"></iframe>
                          </div>
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <div class = "btn_hover2"> <p class="card-text">ピアノソナタ ハ長調 K545</p></div>
                          <p class="card-text"><small class="text-muted">シチュエーション：元気</small></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  
           
             <div class="card mb-3" style="max-width: 80%; height:170px;">
                    <div class="row no-gutters">
                      <div class="col-md-4">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/UJdlnqDiRnE"></iframe>
                        </div>
                        </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <div class = "btn_hover2"> <p class="card-text">ピアノソナタ ハ長調 K545</p></div>
                          <p class="card-text"><small class="text-muted">シチュエーション：元気</small></p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="card mb-3" style="max-width: 80%; height:170px;">
                    <div class="row no-gutters">
                      <div class="col-md-4">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/UJdlnqDiRnE"></iframe>
                        </div>
                        </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <div class = "btn_hover2"> <p class="card-text">ピアノソナタ ハ長調 K545</p></div>
                          <p class="card-text"><small class="text-muted">シチュエーション：元気</small></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  
               
             </div>
         </div>
      </div>
     </div>
            </body>
</html>

                    
                   