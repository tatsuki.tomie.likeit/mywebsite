<%@ page language="java" contentType="text/html;charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<!-- BootstrapのCSS読み込み -->

 <link rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css">
    <link rel ="stylesheet" href="<%=request.getContextPath() %>/css/common.css">

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
<!-- ヘッダー -->
<header>
    <a href= "Mainpage"><h1>みんなの楽譜</h1></a>
        
</header>
	<!-- /header -->

	<!-- body -->

	<br><br><br>

		<div class="row">
			<div class="col-6 offset-3">
				<!-- エラー処理 start -->
				<c:if test ="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg} </div>
			 	</c:if>
				<!--エラー処理 end  -->

				<form action="LoginServlet" method="post">
					<div class="form-group row">
						<label for="inputLoginId" class="col-3 col-form-label">ログインID</label>
						<div class="col-9">
							<input type="text" name="inputLoginId" id="inputLoginId"
								class="form-control" value="${loginId}" autofocus>
						</div>
					</div>

					<div class="form-group row">
						<label for="inputPassword" class="col-3 col-form-label">パスワード</label>
						<div class="col-9">
							<input type="password" name="password" id="inputPassword"
								class="form-control" autofocus>
						</div>
					</div>

					<button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
				</form>
	            <br>
						<p class="right-align">
							<a href="UserAddServlet">新規登録</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- /card-container -->
	</div>
	<!-- /container -->


</body>
</html>