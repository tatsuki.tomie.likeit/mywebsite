<%@ page language="java" contentType="text/html;charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>result</title>
    
    
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/style.css">
    <link rel ="stylesheet" href="<%=request.getContextPath() %>/css/common.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c" rel="stylesheet">
</head>

<body>
    <!-- ヘッダー -->
    <header>
           <a href= "Mainpage"><h1>みんなの楽譜</h1></a>
           <div class ="container-fluid">
           <div class ="row">
           <div class ="col-5 ml-auto">
            <form method="post" action="SearchResult" class="search_container">
                <input type="text" size="25" placeholder="キーワード検索" name= "search_word">
                <input type="submit" value="&#xf002">
              </form>
          </div>
        
         <div class="col-3">
         <ul class= "menu">
			<c:if test="${userInfo != null}">
			<li class="menu-item"><a href="UserData">ユーザー情報</a></li>
			</c:if>
			<c:choose>
			<c:when test="${userInfo != null}">
			<li>&nbsp;&nbsp;</li>
			<li class="menu-item"><a href="LogoutServlet">ログアウト</a></li>
			</c:when>
			<c:otherwise>
			<li class="menu-item"><a href="LoginServlet">ログイン</a></li>
			</c:otherwise>
			</c:choose>
         </ul>
		</div>
		 </div>
		</div>
    </header>
    <div class="container-fluid" style= "background-color: rgba(229, 230, 230, 0.411) ">
    <div class="container mt-5">
      <br></br>
        <h4>${name}${searchWord} 楽譜一覧</h4>
                <p>1~20/400件</p>
                <c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg} </div>
				</c:if>
          		<c:if test="${scoreList !=null}">
          		<c:forEach var="Score" items="${scoreList}">
                <div class="card mb-0" style="max-width: 100%; height:170px;">
                    <div class="row no-gutters">
                      <div class="col-md-4">
                      <iframe width="320" height="168" src= "${Score.movieFile}"
                        title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                        </iframe>
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <p class="card-text"> <a class = "btn_hover2" href = "ResultDetail?id=${Score.id}">${Score.name} </p></a>
                          <p class="card-text">${Score.composerName}</p>
                          <p class="card-text"><small class="text-muted">${Score.situationName}</small><br><small class="text-muted">曲名をクリックすると詳細ページに飛びます</small></p>
                        </div>
                      </div>
                      </div>
                  </div>
                  </c:forEach>
                  </c:if>
                 </div>
                </div>
                 
                  <div class="d-flex justify-content-center">
                    <ul class="pagination">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                        </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                          </ul>
                    </ul>
                </div>
            </div>
        </div>
